package com.javliin.factionstop;

import com.javliin.factionstop.listener.CommandListener;
import com.javliin.factionstop.listener.FactionsListener;
import com.javliin.factionstop.listener.SignListener;
import com.javliin.factionstop.manager.ConfigManager;
import com.javliin.factionstop.manager.FactionTopManager;
import com.javliin.factionstop.value.FactionValue;
import com.javliin.factionstop.value.FactionValueCalculator;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.struct.FFlag;
import org.bukkit.Location;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class FactionsTop extends JavaPlugin {
    private ConfigManager configManager;
    private FactionTopManager factionTopManager;
    private net.milkbowl.vault.economy.Economy economy;

    public void onEnable() {
        saveDefaultConfig();

        configManager = new ConfigManager(getConfig());
        factionTopManager = new FactionTopManager(configManager.getFactionValues(), this);

        new FactionValueCalculator(this);

        getServer().getPluginManager().registerEvents(new CommandListener(this), this);
        getServer().getPluginManager().registerEvents(new FactionsListener(this), this);
        getServer().getPluginManager().registerEvents(new SignListener(this), this);

        FactionValue factionValue;

        for(Faction faction : Factions.i.get()) {
            if (!faction.getFlag(FFlag.PERMANENT) && (configManager.getData("factions") == null || !configManager.getDataList("factions").getKeys(false).contains(faction.getTag()))) {
                factionValue = new FactionValue(faction, 0L, 0L, 0L);

                for (FPlayer fPlayer : faction.getFPlayers())
                    try {
                        factionValue.addToTotal(economy.getBalance(fPlayer.getPlayer()));
                    }catch(NullPointerException exception) {}

                factionTopManager.addFaction(factionValue);
                FactionValueCalculator.addValue(faction);
            }
        }

        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            getLogger().severe("Vault not found; disabling...");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        RegisteredServiceProvider<net.milkbowl.vault.economy.Economy> rsp = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        economy = rsp.getProvider();
    }

    public void onDisable() {
        saveConfig();

        for(Location location : FactionValueCalculator.getChestCache().keySet()) {
            if(location == null || FactionValueCalculator.getChestCache().get(location) == null) continue;

            configManager.setData("chests." + location.getWorld().getName() + "_" + location.getBlockX() + "_" + location.getBlockY() + "_" + location.getBlockZ(),
                FactionValueCalculator.getChestCache().get(location));
        }

        try {
            configManager.getConfigData().save(new File("plugins/FactionsTop/data.yml"));
        }catch(IOException exception) {
            exception.printStackTrace();
        }
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    public FactionTopManager getFactionTopManager() {
        return factionTopManager;
    }

    public net.milkbowl.vault.economy.Economy getEconomy() { return economy; }
}
