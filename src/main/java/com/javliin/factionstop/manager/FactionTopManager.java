package com.javliin.factionstop.manager;

import com.javliin.factionstop.FactionsTop;
import com.javliin.factionstop.value.FactionValue;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class FactionTopManager {
    private List<FactionValue> top;
    private FactionsTop core;

    public FactionTopManager(List<FactionValue> values, FactionsTop core) {
        this.top = organizeList(values);
        this.core = core;
    }

    private List<FactionValue> organizeList(List<FactionValue> values) {
        Collections.sort(values, (FactionValue o1, FactionValue o2) -> o1.getTotal().compareTo(o2.getTotal()));
        Collections.reverse(values);

        return values;
    }

    public List<FactionValue> reOrganizeList(FactionValue oldValue, FactionValue newValue) {
        ListIterator iterator = top.listIterator();

        while(iterator.hasNext()){
            FactionValue factionValue = (FactionValue) iterator.next();

            if(factionValue.getFaction().getTag().equals(oldValue.getFaction().getTag())) {
                iterator.remove();
                break;
            }
        }

        top.add(newValue);

        top = organizeList(top);

        core.getConfigManager().setData("factions." + newValue.getFaction().getTag() + ".containers", newValue.getContainers());
        core.getConfigManager().setData("factions." + newValue.getFaction().getTag() + ".items", newValue.getItems());
        core.getConfigManager().setData("factions." + newValue.getFaction().getTag() + ".spawners", newValue.getSpawners());

        return top;
    }

    public List<FactionValue> addFaction(FactionValue factionValue) {
        top.add(factionValue);

        top = organizeList(top);

        core.getConfigManager().setData("factions." + factionValue.getFaction().getTag() + ".containers", 0);
        core.getConfigManager().setData("factions." + factionValue.getFaction().getTag() + ".items", 0);
        core.getConfigManager().setData("factions." + factionValue.getFaction().getTag() + ".spawners", 0);

        return top;
    }

    public List<FactionValue> removeFaction(FactionValue factionValue) {
        top.remove(factionValue);

        core.getConfigManager().setData("factions." + factionValue.getFaction().getTag(), null);

        return top;
    }

    public List<FactionValue> getList() {
        return top;
    }
}
