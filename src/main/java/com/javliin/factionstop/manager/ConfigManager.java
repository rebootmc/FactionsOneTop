package com.javliin.factionstop.manager;

import com.javliin.factionstop.value.FactionValue;
import com.massivecraft.factions.Factions;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ConfigManager {
    private FileConfiguration config;
    private FileConfiguration data;

    public ConfigManager(FileConfiguration config) {
        this.config = config;

        try {
            File data = new File("plugins/FactionsTop/data.yml");

            if(!data.exists())
                data.createNewFile();

            this.data = YamlConfiguration.loadConfiguration(data);
        }catch(IOException exception) {
            exception.printStackTrace();
        }

    }

    @SuppressWarnings("unchecked")
    public HashMap<Material, Long> getItemValues() {
        HashMap<Material, Long> items = new HashMap<>();

        for(String item : config.getConfigurationSection("items").getKeys(false))
            items.put(Material.valueOf(item.toUpperCase()), config.getLong("items." + item));

        return items;
    }

    @SuppressWarnings("unchecked")
    public HashMap<EntityType, Long> getSpawnerValues() {
        HashMap<EntityType, Long> types = new HashMap<>();

        for(String type : config.getConfigurationSection("spawners").getKeys(false))
            types.put(EntityType.valueOf(type.toUpperCase()), config.getLong("spawners." + type));

        return types;
    }

    @SuppressWarnings("unchecked")
    public HashMap<Material, Long> getContainerValues() {
        HashMap<Material, Long> items = new HashMap<>();

        for(String item : config.getConfigurationSection("containers").getKeys(false))
            items.put(Material.valueOf(item.toUpperCase()), config.getLong("containers." + item));

        return items;
    }

    @SuppressWarnings("unchecked")
    public List<FactionValue> getFactionValues() {
        if(data.get("factions") == null)
            return new ArrayList<>();

        List<FactionValue> factions = new ArrayList<>();
        Iterator iterator = data.getConfigurationSection("factions").getKeys(false).iterator();

        while(iterator.hasNext()) {
            String faction = (String) iterator.next();

            if(Factions.i.getByTag(faction) != null) {
                factions.add(new FactionValue(Factions.i.getByTag(faction),
                    data.getLong("factions." + faction + ".containers"),
                    data.getLong("factions." + faction + ".items"),
                    data.getLong("factions." + faction + ".spawners")));
            }else{
                data.set("factions." + faction, null);
            }
        }

        return factions;
    }

    public void set(String path, Object value) {
        config.set(path, value);
    }
    public void setData(String path, Object value) { data.set(path, value); }

    public Object get(String path) {
        return config.get(path);
    }
    public List<String> getList(String path) { return config.getStringList(path); }
    public Object getData(String path) {
        return data.get(path);
    }
    public ItemStack getDataItemStack(String path) { return data.getItemStack(path); }
    public ConfigurationSection getDataList(String path) { return data.getConfigurationSection(path); }

    public FileConfiguration getConfigData() { return this.data; }
}
