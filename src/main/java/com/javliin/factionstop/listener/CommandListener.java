package com.javliin.factionstop.listener;

import com.javliin.factionstop.FactionsTop;
import com.javliin.factionstop.value.FactionValue;
import com.javliin.factionstop.value.FactionValueCalculator;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.struct.Rel;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.ArrayList;

public class CommandListener implements Listener {
    private FactionsTop core;

    public CommandListener(FactionsTop core) {
        this.core = core;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCommandPreprocess(PlayerCommandPreprocessEvent event) {
        if(!event.getMessage().equalsIgnoreCase("/f top") && !event.getMessage().equalsIgnoreCase("/factions top"))
            return;

        event.setCancelled(true);

        String[] args = event.getMessage().split(" ");

        if(args.length >= 3) {
            try {
                event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', ((String) core.getConfigManager().get("list-title"))
                    .replace("%page%", "" + Integer.parseInt(args[2]))
                    .replace("%pages%",  "" + Math.ceil(core.getFactionTopManager().getList().size() / (Integer) core.getConfigManager().get("list-no")))));

                if(Integer.parseInt(args[2]) > 0) {
                    event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', (String) core.getConfigManager().get("message.number-too-low")));
                    return;
                }

                sendList(FactionValueCalculator.factionToFactionValue(FPlayers.i.get(event.getPlayer()).getFaction()), event.getPlayer(), Integer.parseInt(args[2]));
            }catch(NumberFormatException exception) {
                event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', (String) core.getConfigManager().get("message.not-a-number")));
                return;
            }
            return;
        }

        event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', ((String) core.getConfigManager().get("list-title"))
            .replace("%page%", "1")
            .replace("%pages%", "" + Math.ceil(core.getFactionTopManager().getList().size() / (Integer) core.getConfigManager().get("list-no")))));

        sendList(FactionValueCalculator.factionToFactionValue(FPlayers.i.get(event.getPlayer()).getFaction()), event.getPlayer(), 0);
    }

    private void sendList(FactionValue factionValue, Player player, Integer page) {
        Integer listAmount = (Integer) core.getConfigManager().get("list-no");

        for(int i = (listAmount * page); i < ((listAmount * page) + listAmount); i++) {
            FactionValue value;

            try {
                value = core.getFactionTopManager().getList().get(i);
            }catch(IndexOutOfBoundsException exception) {
                return;
            }

            TextComponent base = new TextComponent(new ComponentBuilder(factionValue.getFaction().getTag()).create());
            ArrayList components = new ArrayList();

            base.addExtra(new TextComponent(ComponentSerializer.parse("{text: \"\n\"}")));

            for(String entry : core.getConfigManager().getList("hover")) {
                base.addExtra(new TextComponent(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', entry)
                    .replace("{faction}", factionValue.getFaction().getTag())
                    .replace("{containers}", factionValue.getContainers().toString())
                    .replace("{spawners}", factionValue.getSpawners().toString())
                    .replace("{blocks}", factionValue.getItems().toString())).create()));

                base.addExtra(new TextComponent(ComponentSerializer.parse("{text: \"\n\"}")));
            }

            components.add(base);

            TextComponent hover = addHover(ChatColor.translateAlternateColorCodes('&', ((String) core.getConfigManager().get("list-value"))
                .replace("%position%", "" + (i + 1))
                .replace("%name%", relationToColor(value.getFaction().getRelationTo(factionValue.getFaction())) + value.getFaction().getTag())
                .replace("%value%", "" + value.getTotal())), (BaseComponent[])components.toArray(new BaseComponent[components.size()]));

            player.spigot().sendMessage(hover);
        }
    }

    private TextComponent addHover(String message, BaseComponent[] hoverText) {
        TextComponent newMessage = new TextComponent(message);

        newMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText));
        return newMessage;
    }

    private ChatColor relationToColor(Rel rel) {
        switch(rel.name()) {
            case("ALLY"):
                return ChatColor.GREEN;
            case("ENEMY"):
                return ChatColor.RED;
            case("TRUCE"):
                return ChatColor.WHITE;
            default:
                return ChatColor.GRAY;
        }
    }
}
