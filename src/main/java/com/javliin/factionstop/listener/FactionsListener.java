package com.javliin.factionstop.listener;

import com.javliin.factionstop.FactionsTop;
import com.javliin.factionstop.value.FactionValue;
import com.javliin.factionstop.value.FactionValueCalculator;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.event.*;
import com.massivecraft.factions.struct.FFlag;
import org.bukkit.Bukkit;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.EnderChest;

import java.util.Set;

public class FactionsListener implements Listener {
    private FactionsTop core;

    public FactionsListener(FactionsTop core) {
        this.core = core;
    }

    @EventHandler
    public void onFactionRename(FactionRenameEvent event) {
        core.getConfigManager().setData("factions." + event.getFactionTag() + ".containers", core.getConfigManager().get("factions." + event.getOldFactionTag() + ".containers"));
        core.getConfigManager().setData("factions." + event.getFactionTag() + ".items", core.getConfigManager().get("factions." + event.getOldFactionTag() + ".items"));
        core.getConfigManager().setData("factions." + event.getFactionTag() + ".spawners", core.getConfigManager().get("factions." + event.getOldFactionTag() + ".spawners"));
        core.getConfigManager().setData("factions." + event.getOldFactionTag(), null);
    }

    @EventHandler
    public void onFactionClaim(LandClaimEvent event) {
        if(event.getFaction().getFlag(FFlag.PERMANENT))
            return;

        core.getFactionTopManager().reOrganizeList(
            FactionValueCalculator.factionToFactionValue(event.getFaction()),
            FactionValueCalculator.updateValue(event.getFaction(), event.getLocation(), "+"));
    }

    @EventHandler
    public void onFactionUnclaim(LandUnclaimEvent event) {
        if(event.getFaction().getFlag(FFlag.PERMANENT))
            return;

        core.getFactionTopManager().reOrganizeList(
                FactionValueCalculator.factionToFactionValue(event.getFaction()),
                FactionValueCalculator.updateValue(event.getFaction(), event.getLocation(), "-"));
    }

    @EventHandler
    public void onFactionUnclaimAll(LandUnclaimAllEvent event) {
        core.getConfigManager().setData("factions." + event.getFactionTag() + ".containers", 0);
        core.getConfigManager().setData("factions." + event.getFactionTag() + ".items", 0);
        core.getConfigManager().setData("factions." + event.getFactionTag() + ".spawners", 0);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onFactionCreate(FactionCreateEvent event) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(core, () -> {
            FactionValue factionValue = new FactionValue(event.getFPlayer().getFaction(), 0L, 0L, 0L);

            for (FPlayer fPlayer : event.getFPlayer().getFaction().getFPlayers())
                try {
                    factionValue.addToTotal(core.getEconomy().getBalance(fPlayer.getPlayer()));
                }catch(NullPointerException exception) {}

            core.getFactionTopManager().addFaction(factionValue);
        }, 5);
    }

    @EventHandler
    public void onFactionDisband(FactionDisbandEvent event) {
        core.getFactionTopManager().removeFaction(FactionValueCalculator.factionToFactionValue(event.getFaction()));
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if(FPlayers.i.get(event.getPlayer()).getFaction().getFlag(FFlag.PERMANENT))
            return;

        if(!Board.getFactionAt(event.getPlayer().getLocation()).equals(FPlayers.i.get(event.getPlayer()).getFaction()))
            return;

        core.getFactionTopManager().reOrganizeList(
                FactionValueCalculator.factionToFactionValue(FPlayers.i.get(event.getPlayer()).getFaction()),
                FactionValueCalculator.updateValue(FPlayers.i.get(event.getPlayer()).getFaction(), event));
    }

    @EventHandler
    public void onBlockDestroy(BlockBreakEvent event) {
        if(FPlayers.i.get(event.getPlayer()).getFaction().getFlag(FFlag.PERMANENT))
            return;

        if(!Board.getFactionAt(event.getPlayer().getLocation()).equals(FPlayers.i.get(event.getPlayer()).getFaction()))
            return;

        core.getFactionTopManager().reOrganizeList(
                FactionValueCalculator.factionToFactionValue(FPlayers.i.get(event.getPlayer()).getFaction()),
                FactionValueCalculator.updateValue(FPlayers.i.get(event.getPlayer()).getFaction(), event));
    }

    @EventHandler
    public void onPlayerJoinFaction(FPlayerJoinEvent event) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(core, () -> {
            FactionValueCalculator.factionToFactionValue(event.getFaction()).addToTotal(core.getEconomy().getBalance(event.getFPlayer().getPlayer()));
        }, 10);
    }

    @EventHandler
    public void onPlayerLeaveFaction(FPlayerLeaveEvent event) {
        Set<FPlayer> fPlayers = event.getFaction().getFPlayers();

        fPlayers.remove(event.getFPlayer());

        if(fPlayers.isEmpty()) {
            core.getFactionTopManager().removeFaction(FactionValueCalculator.factionToFactionValue(event.getFaction()));
            return;
        }

        FactionValueCalculator.factionToFactionValue(event.getFaction()).takeFromTotal(core.getEconomy().getBalance(event.getFPlayer().getPlayer()));
    }

    @EventHandler
    public void onChestClose(InventoryCloseEvent event) {
        if(event.getInventory().getHolder() instanceof Chest || event.getInventory().getHolder() instanceof EnderChest) {
            if(Board.getFactionAt(((Chest)event.getInventory().getHolder()).getLocation()).getFlag(FFlag.PERMANENT))
                return;

            FactionValue oldValue = FactionValueCalculator.factionToFactionValue(Board.getFactionAt(((Chest)event.getInventory().getHolder()).getLocation()));
            FactionValue newValue = oldValue;

            if(oldValue.getFaction().getFlag(FFlag.PERMANENT))
                return;

            long value = 0;
            long value_ = 0;

            for(ItemStack item : FactionValueCalculator.getChestCache().get(((Chest)event.getInventory().getHolder()).getLocation())) {
                if(item == null || core.getConfigManager().getContainerValues().get(item.getType()) == null) continue;

                if(item.getAmount() != 0) {
                    value = value + core.getConfigManager().getContainerValues().get(item.getType()) * item.getAmount();
                }else{
                    value = value + core.getConfigManager().getContainerValues().get(item.getType());
                }
            }

            for(ItemStack item : event.getInventory().getContents()) {
                if(item == null || core.getConfigManager().getContainerValues().get(item.getType()) == null) continue;

                value_ = value_ + core.getConfigManager().getContainerValues().get(item.getType()) * item.getAmount();
            }

            FactionValueCalculator.getChestCache().put(((Chest)event.getInventory().getHolder()).getLocation(), event.getInventory().getContents());
            newValue.takeFromContainers(value);
            newValue.addToContainers(value_);
            core.getFactionTopManager().reOrganizeList(oldValue, newValue);
        }

        if(event.getInventory().getHolder() instanceof DoubleChest) {
            if(Board.getFactionAt(((DoubleChest)event.getInventory().getHolder()).getLocation()).getFlag(FFlag.PERMANENT))
                return;

            FactionValue oldValue = FactionValueCalculator.factionToFactionValue(Board.getFactionAt(((DoubleChest)event.getInventory().getHolder()).getLocation()));
            FactionValue newValue = oldValue;

            if(oldValue.getFaction().getFlag(FFlag.PERMANENT))
                return;

            long value = 0;
            long value_ = 0;

            for(ItemStack item : FactionValueCalculator.getChestCache().get(((DoubleChest) event.getInventory().getHolder()).getLocation())) {
                if(item == null || core.getConfigManager().getContainerValues().get(item.getType()) == null) continue;

                if(item.getAmount() != 0) {
                    value = value + core.getConfigManager().getContainerValues().get(item.getType()) * item.getAmount();
                }else {
                    value = value + core.getConfigManager().getContainerValues().get(item.getType()) * item.getAmount();
                }
            }

            for(ItemStack item : event.getInventory().getContents()) {
                if(item == null || core.getConfigManager().getContainerValues().get(item.getType()) == null) continue;

                value_ = value_ + core.getConfigManager().getContainerValues().get(item.getType()) * item.getAmount();
            }

            FactionValueCalculator.getChestCache().put(((DoubleChest)event.getInventory().getHolder()).getLocation(), event.getInventory().getContents());

            newValue.takeFromContainers(value);
            newValue.addToContainers(value_);
            core.getFactionTopManager().reOrganizeList(oldValue, newValue);
        }
    }
}
