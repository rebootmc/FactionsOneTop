package com.javliin.factionstop.listener;

import com.javliin.factionstop.FactionsTop;
import com.javliin.factionstop.value.FactionValue;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class SignListener implements Listener {
    private FactionsTop core;

    public SignListener(FactionsTop core) {
        this.core = core;
    }

    @EventHandler
    public void onSignPlace(SignChangeEvent event) {
        if(!event.getLine(0).equalsIgnoreCase("[factionstop]"))
            return;

        if(!event.getPlayer().hasPermission("factionstop.sign"))
            return;

        try {
            FactionValue factionValue =  core.getFactionTopManager().getList().get(Integer.parseInt(event.getLine(1)) - 1);
            event.setLine(0, factionValue.getFaction().getTag());
            event.setLine(1, "Worth / Leader");
            event.setLine(2, "" + factionValue.getTotal());
            event.setLine(3, factionValue.getFaction().getFPlayerLeader().getName());
        }catch(IndexOutOfBoundsException | NumberFormatException | NullPointerException exception) {}
    }
}
