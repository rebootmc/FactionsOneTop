package com.javliin.factionstop.value;

import com.javliin.factionstop.FactionsTop;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.TerritoryAccess;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.block.DoubleChest;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.*;

public class FactionValueCalculator {
    private static FactionsTop core;
    private static Material[] containers = new Material[]{Material.CHEST, Material.ENDER_CHEST, Material.TRAPPED_CHEST};
    private static HashMap<Location, ItemStack[]> chestCache = new HashMap<>();

    public FactionValueCalculator(FactionsTop core_) {
        core = core_;

        if(core.getConfigManager().getData("chests") == null)
            return;

        String[] location;
        ItemStack[] items;
        Location location_;

        for(String chest : core.getConfigManager().getDataList("chests").getKeys(false)) {
            location = chest.split("_");
            location_ = new Location(Bukkit.getWorld(location[0]), Double.parseDouble(location[1]), Double.parseDouble(location[2]), Double.parseDouble(location[3]));
            items = new ItemStack[core.getConfigManager().getDataList("chests." + chest).getKeys(false).size()];

            for(int i = 0; i < core.getConfigManager().getDataList("chests." + chest).getKeys(false).size(); i++) {
                items[i] = core.getConfigManager().getDataItemStack("chests." + chest);
            }

            chestCache.put(location_, items);
        }

        core.getConfigManager().setData("chests", null);
    }

    @Deprecated
    public static FactionValue addValue(Faction faction) {
        FactionValue oldValue = factionToFactionValue(faction);
        FactionValue tempValue;

        for(FLocation fLocation : getFactionClaims(faction)) {
            core.getFactionTopManager().reOrganizeList(oldValue, (tempValue = updateValue(faction, fLocation, "+")));
            oldValue = tempValue;
        }

        return factionToFactionValue(faction);
    }

    public static FactionValue updateValue(Faction faction, BlockBreakEvent event) {
        return updateValue(faction, event.getBlock(), "-");
    }

    public static FactionValue updateValue(Faction faction, BlockPlaceEvent event) {
        return updateValue(faction, event.getBlock(), "+");
    }

    public static FactionValue updateValue(Faction faction, FLocation fLocation, String operation) {
        Chunk chunk = fLocation.getWorld().getChunkAt((int) fLocation.getX(), (int) fLocation.getZ());
        FactionValue factionValue = factionToFactionValue(faction);

        int x = chunk.getX() << 4;
        int z = chunk.getZ() << 4;

        World world = chunk.getWorld();

        for(int xx = x; xx < x + 16; xx++) {
            for (int zz = z; zz < z + 16; zz++) {
                for (int yy = 0; yy < 256; yy++) {
                    factionValue = updateValue(factionValue, faction, world.getBlockAt(xx, yy, zz), operation);
                }
            }
        }

        return factionValue;
    }

    public static FactionValue factionToFactionValue(Faction faction) {
        List<FactionValue> factionValues = core.getConfigManager().getFactionValues();

        for(FactionValue valueFaction : factionValues) {
            if (valueFaction.getFaction().getTag().equals(faction.getTag())) {
                return factionValues.get(factionValues.indexOf(valueFaction));
            }
        }

        return null;
    }

    public static HashMap<Location, ItemStack[]> getChestCache() { return chestCache; }

    private static FactionValue updateValue(Faction faction, Block block, String operation) {
        return updateValue(factionToFactionValue(faction), faction, block, operation);
    }

    private static FactionValue updateValue(FactionValue factionValue, Faction faction, Block block, String operation) {
        long containers_ = factionValue.getContainers();
        long items = factionValue.getItems();
        long spawners = factionValue.getSpawners();
        boolean add = operation.equals("+");

        if(block.getType() == Material.MOB_SPAWNER && core.getConfigManager().getSpawnerValues().get(((CreatureSpawner) block.getState()).getSpawnedType()) != null)
            if(add) {
                spawners = spawners + core.getConfigManager().getSpawnerValues().get(((CreatureSpawner) block.getState()).getSpawnedType());
            }else{
                spawners = spawners - core.getConfigManager().getSpawnerValues().get(((CreatureSpawner) block.getState()).getSpawnedType());
            }


        for(Material material : containers) {
            if(block.getType() != material) continue;

            if(block.getState() instanceof Chest) {
                if(((Chest)block.getState()).getInventory().getHolder() instanceof DoubleChest) {
                    chestCache.put(((DoubleChest) (((Chest) block.getState()).getInventory().getHolder())).getLocation(), ((Chest) block.getState()).getInventory().getContents());
                }else{
                    chestCache.put(block.getLocation(), ((Chest) block.getState()).getInventory().getContents());
                }

                for (ItemStack item : ((Chest) block.getState()).getInventory().getContents()) {
                    if (item == null || core.getConfigManager().getContainerValues().get(item.getType()) == null) continue;

                    if (add) {
                        containers_ = containers_ + core.getConfigManager().getContainerValues().get(item.getType()) * item.getAmount();
                    } else {
                        containers_ = containers_ - core.getConfigManager().getContainerValues().get(item.getType()) * item.getAmount();
                        chestCache.put(block.getLocation(), null);
                    }
                }
            }
        }

        if(core.getConfigManager().getItemValues().get(block.getType()) != null)
            if(add) {
                items = items + core.getConfigManager().getItemValues().get(block.getType());
            }else{
                items = items - core.getConfigManager().getItemValues().get(block.getType());
            }

        return new FactionValue(faction, containers_, items, spawners);
    }

    @SuppressWarnings("unchecked")
    private static Set<FLocation> getFactionClaims(Faction faction) {
        Field map;
        HashMap<FLocation, TerritoryAccess> mapObject;

        try {
            map = Board.class.getDeclaredField("flocationIds");

            map.setAccessible(true);
            mapObject = (HashMap<FLocation, TerritoryAccess>) map.get(null);
            map.setAccessible(false);
        }catch(NoSuchFieldException | IllegalAccessException exception) {
            exception.printStackTrace();
            return new HashSet<>();
        }

        Set<FLocation> claims = new HashSet<>();
        Iterator<Map.Entry<FLocation, TerritoryAccess>> iter = mapObject.entrySet().iterator();

        while(iter.hasNext()) {
            Map.Entry<FLocation, TerritoryAccess> entry = iter.next();

            if (entry.getValue().getHostFactionID().equals(faction.getId()))
                claims.add(entry.getKey());
        }

        return claims;
    }
}
