package com.javliin.factionstop.value;

import com.massivecraft.factions.Faction;

public class FactionValue {
    private Faction faction;
    private Long total;
    private Long containers;
    private Long items;
    private Long spawners;
    private Long control;

    public FactionValue(Faction faction, Long containers, Long items, Long spawners) {
        this.faction = faction;
        this.containers = containers;
        this.items = items;
        this.spawners = spawners;
        this.control = 0L;

        updateTotal();
    }

    private void updateTotal() { this.total = containers + items + spawners + control; }

    public void setFaction(Faction faction) {
        this.faction = faction;
    }

    public void setContainers(Long containers) {
        this.containers = containers;
        this.updateTotal();
    }

    public void setItems(Long items) {
        this.items = items;
        this.updateTotal();
    }

    public void setSpawners(Long spawners) {
        this.spawners = spawners;
        this.updateTotal();
    }

    public void addToTotal(double amount) {
        control = control + (long) amount;
        updateTotal();
    }

    public void addToContainers(long amount) {
        containers = containers + amount;
        updateTotal();
    }

    public void takeFromContainers(long amount) {
        containers = containers - amount;
        updateTotal();
    }

    public void takeFromTotal(double amount) {
        control = control - (long) amount;
        updateTotal();
    }

    public Faction getFaction() { return this.faction; }
    public Long getTotal() {
        return this.total;
    }
    public Long getContainers() {
        return this.containers;
    }
    public Long getItems() {
        return this.items;
    }
    public Long getSpawners() {
        return this.spawners;
    }
}
